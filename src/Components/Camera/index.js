import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Button } from 'react-native';
import { RNCamera } from 'react-native-camera';
import styles from './styles';

export class Camera extends Component {

    camera = null;
    state = {
        buttonText: 'SNAP',
    }

    takePicture = async () => {
        if (this.camera && this.state.buttonText == 'SNAP') {
            const options = { quality: 1, base64: true, doNotSave: false, pauseAfterCapture: true };
            const data = await this.camera.takePictureAsync(options);
            this.setState({ buttonText: 'CONTINUE' });
        } else {
            this.setState({ buttonText: 'SNAP' });
            this.camera.resumePreview();
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={(ref) => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    onGoogleVisionBarcodesDetected={({ barcodes }) => {
                        console.log(barcodes);
                    }}
                />
                <TouchableOpacity style={{ position: 'absolute', left: 10, top: 20, padding: 10, backgroundColor: 'white' }}
                    onPress={() => { this.props.navigation.goBack() }}>
                    <Text>BACK</Text>
                </TouchableOpacity>
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                        <Text style={{ fontSize: 14 }}> { this.state.buttonText } </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default Camera
