import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import styles from './styles';
import MainStyles from '../MainStyles';

import FingerPrint from '../../Assets/fingerprint.png';
import Camera from '../../Assets/camera.png';
import Micro from '../../Assets/micro.png';

const BUTTONS = [
    // { id: 1, text: 'Scan finger', icon: FingerPrint },
    { id: 2, text: 'Face detection', icon: Camera, route: 'Camera' },
    { id: 3, text: 'Voice detection', icon: Micro },
];

export class Home extends Component {

    navigateTo = (route) => {
        if (route) {
            this.props.navigation.navigate(route);
        }
    }

    render() {
        return (
            <View style={ styles.container }>
                <Text style={ styles.title }>Biometric check-in</Text>
                { BUTTONS.map(button => {
                    return (
                        <TouchableOpacity key={ button.id } style={[ MainStyles.card, styles.button ]} onPress={() => { this.navigateTo(button.route) }}>
                            <Image source={ button.icon } style={ styles.buttonIcon } />
                            <Text style={ styles.buttonText }>{ button.text }</Text>
                        </TouchableOpacity>
                    )
                }) }
            </View>
        )
    }
}

export default Home;
