import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#f8f8f8'
    },
    buttonIcon: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    button: {
        flexDirection: 'row',
        margin: 20,
        justifyContent: 'flex-start',
        padding: 20,
    },
    buttonText: {
        marginLeft: 20,
        fontSize: 18,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 20,
    },
});

export default styles;
