const MainConstants = {
    MainColor: '#B22080',
    PrimaryColor: '#007bff',
    SuccessColor: '#28a745',
    WarningColor: '#ffc107',
    DangerColor: '#dc3545',
    InfoColor: '#17a2b8',
    BorderColor: '#ced4da',
    DarkTextColor: '#495057',
    DarkBlueColor: '#017680',
    DisableColor: '#6c757d',
    LightPinkColor: '#d10aa1',
    CyanColor: '#00d2e9',
    MainURL: 'https://sportymall.app.ftcjsc.com',
    // MainURL: 'https://spmall.app3.ftcjsc.com',
    
    AuthenticatedUserStorageName: 'SportyMallAuthenticatedUser',
    CitiesStorageName: 'SportyMallCities',
    DistrictsStorageName: 'SportyMallDistricts',
    WardsStorageName: 'SportyMallWards',
    LanguageStorageName: 'SportiveMallLanguage',
    HelpUrlStorageName: 'SportiveMallHelpUrl',

    HealthKitNotDetermined: 'NotDetermined',
    HealthKitDenied: 'SharingDenied',
    HealthKitAuthorized: 'SharingAuthorized',

    ChartDate: 31,
    FCMTopicPrefix: 'sportivemall_',
    // FCMTopicPrefix: 'sportivemalldev_',
};

export default MainConstants;
