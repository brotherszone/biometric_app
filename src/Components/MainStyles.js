import { StyleSheet } from 'react-native';
import MainConstants from './MainConstants.js';

const MainStyles = StyleSheet.create({
    button: {
        textAlign: 'center',
        padding: 10,
        borderRadius: 5,
        paddingLeft: 25,
        paddingRight: 25,
        backgroundColor: '#f8f9fa',
        borderWidth: 1,
        borderColor: '#f8f9fa',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    smallButton: {
        textAlign: 'center',
        padding: 5,
        borderRadius: 5,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#f8f9fa',
        borderWidth: 1,
        borderColor: '#f8f9fa',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    roundButton: {
        borderRadius: 20,
    },
    primaryButton: {
        backgroundColor: MainConstants.PrimaryColor,
        borderColor: MainConstants.PrimaryColor,
    },
    successButton: {
        backgroundColor: MainConstants.SuccessColor,
        borderColor: MainConstants.SuccessColor,
    },
    warningButton: {
        backgroundColor: MainConstants.WarningColor,
        borderColor: MainConstants.WarningColor,
    },
    disableButton: {
        backgroundColor: MainConstants.DisableColor,
        borderColor: MainConstants.DisableColor,
    },
    whiteText: {
        color: 'white',
        fontFamily: 'UTM Avo',
    },
    card: {
        shadowOffset: {width: 0, height: 2},
        shadowColor: '#ccc',
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 2,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default MainStyles;
